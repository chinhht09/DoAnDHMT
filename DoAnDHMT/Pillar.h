﻿#pragma once

#include "Dependencies/freeglut/freeglut.h"

//Vẽ ống nước
class Pillar {
	private:
		//Khai báo biến theo giá trị:
		//PosX là tọa độ của ống nước theo Ox
		//PosY là tọa độ của ống nước theo Oy
		// Width là chiều rộng của ống nước
		//Height là chiều cao của ống nước
		int posX, posY, width, heigh;
	public:
		Pillar(); // khởi tạo Constructor Không tham số
		Pillar(int x, int y, int w, int h); // Khởi tạo Constructor với tham số là x,y,w,h
		void render(); // Vẽ ống nước ra màn hình
		int getPosX(); // Lấy giá trị theo tọa độ x của ống nước trên hệ trục tọa độ Ox 
		int getPosY(); // Lấy giá trị theo tọa độ y của ống nước trên hệ trục tọa độ Oy
		int getWidth(); // Lấy giá trị chiều rộng cho ống nước
		int getHeight(); // lấy giá trị chiều cao cho ống nước
		void setPosX(int x); // Gán tọa độ của ống nước theo giá trị x
		void setPosY(int y); // Gán tọa độ của ống nước theo giá trị y
		void setWidth(int w); // Gán chiều rộng của ống nước
		void setHeight(int h); // Gán chiều cao của ống nước
};


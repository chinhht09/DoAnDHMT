﻿#include "game.h"
#include <sys/timeb.h>
#include <sys/utime.h>
#include <math.h>
void game::init() {
	glClearColor(0.1992, 0.5976, 0.7969, 1); // vẽ background cho game có thể thao đổi tùy thích.
	point = 0; //Khởi tạo điểm số của người chơi
	mark = "mark";

	b = new Bird(150, 400, 30, 30); // Khởi tạo chim với tọa 150, 400 và chiều rộng = 30, chiều cao = 30
	b1 = new Bird(100, 300, 15, 15); // khởi tạo đối tượng chim bay phía sau

	ob[0] = new Obstacle(400, 300); // Đối tượng vật cản 1: tọa độ (400, 300)
	ob[1] = new Obstacle(650, 250);// Đối tượng vật cản 2: tọa độ (600, 270)
	ob[2] = new Obstacle(800, 260);// Đối tượng vật cản 3: tọa độ (800, 290)
	ob[3] = new Obstacle(1000, 320);// Đối tượng vật cản 4: tọa độ (1000, 320)
}
// Hàm đếm đến mili giây
int getMilliCount() {
	timeb tb;
	ftime(&tb);
	int nCount = tb.millitm + (tb.time & 0xfffff) * 1000;
	return nCount;
}
// Hàm cho máy nghỉ đến sleeptime milisecond
void sleep(int sleeptime)
{
	int count = 0;
	int beginsleep = getMilliCount();
	while (getMilliCount() - beginsleep < sleeptime)
	{
		count++;
	}
}
// Hàm xác định chương trình sẽ skip frame hay cho máy ngủ
void game::CountFrame() {
	int beginFrame = getMilliCount();	// Thời điểm bắt đầu chu kỳ lặp
	glutPostRedisplay();	// Thông báo khi vẽ lại
	long timeDiff = getMilliCount() - beginFrame;	// thời gian thực hiện xong chu kỳ lặp
	int sleepTime = (int)((1000 / fps) - timeDiff);	// Số mili giây còn dư
	int framesSkipped = 0;	// Số lượng khung hình đã bỏ qua

	
	if (sleepTime > 0) 
	{
		//Nếu máy dựng được nhiều fps hơn 32fps thì trong mili giây còn lại máy sẽ Sleep()
		sleep(sleepTime);
	} 
	// Skip frame nếu máy không dựng đủ 32fps, skip tối đa 5 frame
	while (sleepTime < 0 && framesSkipped <= 5) {
		display();
		sleepTime += (1000 / fps);
		framesSkipped++;
	}
	if (framesSkipped >= 5) {
		// Nếu quá 5 frame bị skip thì thoát game
		exit(2);
		printf("Máy cùi nên bị thoát");
	}
}
void game::reshape(int w, int h) {
	glViewport(0, 0, w, h);//Thiết lập màn hình

	glMatrixMode(GL_PROJECTION);//Ma trận hệ tọa độ
	glLoadIdentity(); //Load ma trận

	glOrtho(0, 600, 0, 600, 10, 0);//Phép chiếu trực giao
}

void game::display() {
	glClear(GL_COLOR_BUFFER_BIT);

	if (isFirstStart) { // Nếu Trạng thái isFristStart là true thì a game sẽ được bắt đầu.
		startGame(); //  Kiểm tra trạng thái xong thì sẽ Start game.
	}

	b->update(); // Hàm update lại đối tượng chim.
	b->render(); // Hàm render đối tượng chim.

	b1->update(); // hàm update đối tượng chim b1
	b1->render();	// Hàm render đối tượng chim b1.

	for (int i = 0; i < 4; i++) { // Vòng lặp vật cản
		ob[i]->update(); // cập nhật vật cản
		ob[i]->render(); // vẽ lại vật cản
		if (ob[i]->getActive() == true) { // Kiểm tra nếu đối tượng vật cản.
			//Kiểm tra xem đối tượng chim có va chạm với vật cản trên hoặc vật cản dưới hay không.
			if (collisionWithTop(ob[i]->getTop(), b) || collisionWithBottom(ob[i]->getBottom(), b)) {
				// Nếu va chạm thì đối tượng chim sẽ được set trạng thái là die.
				b->setDead(true);
			}
			// Nếu tọa độ của đối tượng X + Giá trị đối tượng vật cản ở dưới
			if ((ob[i]->getPosX() + ob[i]->getBottom()->getWidth() + 5) == b->getPosX()) {
				//Nếu đối tượng chim vượt qua vật cản thành công thì sẽ được tăng điểm khi vượt vật cản thành công
				if (b->getDead() == false) {
					point++;
				}
			}
		}
		// Nếu đối tượng chim bị die thì trò chơi sẽ dừng lại.
		if (b->getDead()) {
			ob[i]->setSpeed(0);
		}
	}
	drawPoint();// Hiển thị điểm lên.
	gameOver(); // Kết thúc trò chơi.

	glutSwapBuffers();
	glFlush();
}

void game::timer(int) {
	glutPostRedisplay(); // Báo vẽ lại hàm Display().
	glutTimerFunc(1000 / fps, timer, 0); // Hẹn giờ gọi lệnh tiếp theo 
}

 
void game::inputProcess(unsigned char key, int, int) {
	//Hàm xử lý bàn phím. Kiếm tra đối tượng chim đã die hay chưa và nút ấn 'Space'
	if (!b->getDead() && key == 32) {
		if (isFirstStart) {
			isFirstStart = false;
			b->setSpeed(1.0f); // tốc độ di chuyển của đối tượng chim.
			for (int i = 0; i < 4; i++) {
				ob[i]->setSpeed(5 + (point/10));// Tốc độ di chuyển cửa đổi tượng vật cản.
			}
			glutIdleFunc(CountFrame);

		}
		else {
			b->setVelocity(-9); // set độ nảy lên của đối tượng chim
			glutIdleFunc(CountFrame);
		}
	}
	else if (!b->getDead() && key == 'w') {
		b1->setPosY((int)(b->getPosY()));
		b->setPosY((int)(b->getPosY() + 20.0f));
		glutIdleFunc(CountFrame);

	}
	else if (!b->getDead() && key == 's') {
		b1->setPosY((int)(b->getPosY()));
		b->setPosY((int)(b->getPosY() - 20.0f));
		glutIdleFunc(CountFrame);
	}
	else if (key == 27) {
		exit(0);
		// Nếu chim đã chết thì nhấn bàn phím để chơi lại
	}
	else if (b->getDead()) {
		isFirstStart = true;
		init();
	}
}

// Hàm hiển thị điểm lên màn hình.
void game::drawPoint() {
	glColor3f(1, 1, 17);
	mark = "Score: " + to_string(point);
	glRasterPos2i(250, 580); // Xuất ra màn hình tại tọa độ (250,580)
	for (size_t i = 0; i < mark.size(); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, mark[i]); // Đê hiển thị chuỗi mark lên màn hình với font chữ là Time new roman kích thước 24.
	}
}

// Hàm bắt đầu game
void game::startGame() {
	glColor3f(0.8745, 0, 0.1608);
	string g = "PRESS SPACE TO START GAME";
	glRasterPos2i(125, 290);
	for (size_t i = 0; i < g.size(); i++) {
		glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, g[i]);// Đê hiển thị chuỗi g lên màn hình với font chữ là Time new roman kích thước 24.
	}
}

// Hàm được gọi khi đối tượng chim die
void game::gameOver() {
	if (b->getDead()) {
		glColor3f(1, 1, 0);
		string g = "YOU LOSED";
		glRasterPos2i(250, 300);
		for (size_t i = 0; i < g.size(); i++) {
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, g[i]); // Để hiện thị chuỗi g lên màn hình khi đối tượng chim die
		}

		mark = "You Score is: " + to_string(point);
		glRasterPos2i(100, 270);
		for (size_t i = 0; i < mark.size(); i++) {
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, mark[i]); // Để hiện thị chuỗi mark lên màn hình khi đối tượng chim die
		}
		if (highestPoint < point) {
			highestPoint = point;
		}
		mark = "Highest Score is: " + to_string(highestPoint);
		glRasterPos2i(350, 270);
		for (size_t i = 0; i < mark.size(); i++) {
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, mark[i]); // Để hiện thị chuỗi mark lên màn hình khi đối tượng chim die
		}

		g = "PRESS ANY BUTTON TO RESTART";
		glRasterPos2i(120, 230);
		for (size_t i = 0; i < g.size(); i++) {
			glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, g[i]); // Để hiện thị chuỗi g lên màn hình khi đối tượng chim die
		}
	}
}

// Hàm xử lý va chạm ở trên của đối tượng chim và đối tượng ống nước
bool game::collisionWithTop(Pillar *p, Bird *b) {
	if (b->getPosX() + b->getWidth() < p->getPosX()) return false; // Tọa độ X + chiều rộng của đối tượng chim  bé hơn tọa độ x của đối tượng ống nước
	if ((b->getPosY() + b->getHeight() + 2) < p->getPosY()) return false;  // Tọa độ y + chiều cao của đối tượng chim + 2  bé hơn tọa độ y của đối tượng ống nước
	if (b->getPosX() > (p->getPosX() + p->getWidth())) return false; // Tọa độ x của đối tượng chim lơn hơn tọa độ X + chiều rộng của đối tượng ống nước
	return true; // Trả về kết quả true
}
// Hàm xử lý va chạm ở dưới của đối tượng chim và đối tượng ống nước
bool game::collisionWithBottom(Pillar *p, Bird *b) {
	if (b->getPosX() + b->getWidth() < p->getPosX()) return false; //Tọa độ X + chiều rộng của đối tượng chim bé hơn tạo độ x của đối tượng ống nước
	if (b->getPosX() > p->getPosX() + p->getWidth()) return false; //Tọa độ X của đối tượng X lơn hơn tọa độ X + chiều rộng của đối tượng ống nước  
	if (b->getPosY() > p->getPosY()) return false; // Tọa độ Y của đối tượng chim lơn hơn tọa độ Y của đối tượng ống nước.
	return true; // Trả về kết quả true
}


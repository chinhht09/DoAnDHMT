﻿#include<iostream>
#include"Bird.h"

Bird::Bird(int x, float y, int w, int h) {
	this->posX = x;
	this->posY = y;
	this->width = w;
	this->height = h;
	this->velocity = 0;
	this->speed = 0;
	this->isDead = false;
}

Bird::~Bird() {}

void Bird::setPosX(int x) {
	this->posX = x;
}

void Bird::setPosY(int y) {
	this->posY = y;
}

void Bird::setVelocity(float v) {
	this->velocity = v;
}

void Bird::setDead(bool d) {
	this->isDead = d;
}

void Bird::setSpeed(float s) {
	this->speed = s;
}

int Bird::getPosX() {
	return this->posX;
}

float Bird::getPosY() {
	return this->posY;
}

float Bird::getVelocity() {
	return this->velocity;
}

int Bird::getWidth() {
	return this->width;
}

int Bird::getHeight() {
	return this->height;
}

bool Bird::getDead() {
	return this->isDead;
}

void Bird::update() {
	// Chim bay tới
	//velocity += speed;

	// Chim bay hướng xuống
	//posY -= velocity;

	//Nếu vị trí theo trục Oy của chim <10 hoặc >600 thì chim chết
	//if (this->posY < 10 || this->posY > 600) isDead = true;
	// Không cho chim rơi sâu hơn 10
	//if (this->posY < 10) this->posY = 10;

}


void Bird::render() {
	glColor3f(1, 0, 0); // vẽ chim màu đỏ
	/* Vẽ thân chim hình chữ nhật có toạ độ 4 đỉnh tương ứng
	 là (posX, posY), (posX + width, posY), (posX + width, posY + height),
	 (posX, posY + height)*/
	glRectf(posX, posY, posX + width, posY + height);
	/* Vẽ cánh dưới của chú chim hình chữ nhật có toạ độ tương ứng như vẽ thân*/
	glRectf(posX + width / 4, posY - height / 4, posX + width / 4 + width / 2, posY);
	/* Vẽ cánh trên của chú chim hình chữ nhật có toạ độ tương ứng như vẽ thân*/
	glRectf(posX + width / 4, posY + height + height / 4, posX + width / 4 + width / 2, posY);
	/*Vẽ mỏ của chú chim có hình tam giác*/
	glBegin(GL_TRIANGLES);
	glVertex2i(posX + width, posY + 5);
	glVertex2i(posX + width, posY + height / 2 + 5);
	glVertex2i(posX + width + width / 3, posY + height / 4 + 5);
	glEnd();
}

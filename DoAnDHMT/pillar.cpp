﻿#include "Pillar.h"

// Khởi tạo Constructor

Pillar::Pillar() {}

Pillar::Pillar(int x, int y, int w, int h) {
	this->posX = x;
	this->posY = y;
	this->width = w;
	this->heigh = h;

}
// Khởi tạo phương thức getter, setter
int Pillar::getPosX() {
	return this->posX;
}

int Pillar::getPosY() {
	return this->posY;
}

int Pillar::getWidth() {
	return this->width;
}

int Pillar::getHeight() {
	return this->heigh;
}

void Pillar::setPosX(int x) {
	this->posX = x;
}

void Pillar::setPosY(int y) {
	this->posY = y;
}

void Pillar::setWidth(int w) {
	this->width = w;
}

void Pillar::setHeight(int h) {
	this->heigh = h;
}
// Hàm xuất ra màn hình
void Pillar::render() {
	glColor3f(0, 1, 0); // Gán màu cho ống nước.
	glRectf(posX, posY, posX + width, posY + heigh); // Vẽ ống nước dựa trên tọa độ x, tọa độ y, tọa độ x + Chiều rộng, Tọa độ Y  +  chiều cao
}
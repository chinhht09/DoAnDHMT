﻿#pragma once

#include "Dependencies/freeglut/freeglut.h"

class Bird {
	// Khai báo biến
	private:
		int posX, width, height; //vị trí theo trục Ox, chiều rộng, chiều cao của đối tượng chim
		float posY, velocity, speed; // vị trí theo trục Oy, vận tốc của đối tượng chim
		bool isDead;  // Trạng thái alive hay die của đối tượng chim
	public:
		Bird(int x, float y, int w, int h); //Gán tọa độ hiện tại của chim: vị trí, chiều dài, chiều rộng
		~Bird();
		void setPosX(int x); // Gán vị trí theo trục Ox
		void setPosY(int y); // Gán vị trí theo trục OY
		void setVelocity(float v); // Gán độ nảy của đối tượng chim
		void setDead(bool isDead);// Gán trạng thái alive hay die của đối tượng chim
		void setSpeed(float s); //Gán tốc độ của chim
		bool getDead(); // Lấy trạng thái (alive hay die) của chim
		int getPosX(); // Lấy vị trị theo trục Ox
		float getPosY(); // Lấy vị trí theo trục Oy
		float getVelocity();  // Lấy độ nảy hiện tại của chim
		int getWidth(); // Lấy chiều rộng của chim
		int getHeight();  // Lấy chiều cao của chim
		void update(); // update trạng thái của chim
		void render(); // Biểu thị tương ứng lên màn hình
};
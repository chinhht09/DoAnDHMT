﻿#pragma once

#include<cstdlib>
#include<ctime>

#include"Pillar.h"

//một cặp ống tạo thành chướng ngại vật
class Obstacle {
	private:
		bool isActive; // Cờ hiệu lực của chướng ngại vật (biểu thị cho việc ống còn hiển thị không)
		int speed; // vận tốc của chướng ngại vật
		int posX, posY; // vị trí của vật chướng ngại vật
		Pillar *top; // ống trên
		Pillar *bottom; // ống dưới
	public:
		Obstacle();
		Obstacle(int x, int y);  // Khởi tạo chướng ngại vật
		~Obstacle();
		int random(int min, int max); // Tạo ngẫu nhiên chướng ngại vật
		void setActive(bool a); // Gán cờ hiệu lực
		void setSpeed(int s); // Gán tốc độ của chướng ngại vật
		int getPosX(); // Lấy giá trị của chướng ngại vật theo truc Ox
		int getPosY();  // Lấy gía trị của chướng ngại vật theo trục Oy
		bool getActive();  // Lấy cờ hiệu lực
		Pillar* getTop();  // Lấy ống trên
		Pillar* getBottom();  // Lấy ống dưới
		void update(); // Cập nhật chướng ngại vật
		void render(); // Hiển thị chướng ngại vật ra màn hình
};

﻿#include"Obstacle.h"

Obstacle::Obstacle() {}

Obstacle::Obstacle(int x, int y) {
	// Khởi tạo chướng ngại vật
	this->posX = x;
	this->posY = y;
	isActive = false;
	speed = 0;
	top = new Pillar(x, y + 60, 50, 400);
	bottom = new Pillar(x, y - 60, 50, -400);
}

Obstacle::~Obstacle() {
	delete top;// xóa vật cản ở phía trên
	delete bottom;  // xóa vật cản ở phía dưới
}

int Obstacle::random(int min, int delta) {
	//srand(time(NULL));
	return min + 2 * (rand() % delta + 1);
}

void Obstacle::setActive(bool a) {
	this->isActive = a;
}

void Obstacle::setSpeed(int s) {
	this->speed = s;
}

bool Obstacle::getActive() {
	return this->isActive;
}

int Obstacle::getPosX() {
	return this->posX;
}

int Obstacle::getPosY() {
	return this->posY;
}

Pillar* Obstacle::getTop() {
	return this->top;
}

Pillar* Obstacle::getBottom() {
	return this->bottom;
}

void Obstacle::update() {
	// Vị trí của chướng ngại vật lùi lại
	this->posX -= speed;
	top->setPosX(this->posX);
	bottom->setPosX(this->posX);

	// Nếu chướng ngại vật A có vị trí theo Ox 100<= A <=200 thì vẫn hiển thị và ngược lại
	if (this->posX <= 250 && this->posX >= 50) isActive = true;
	else isActive = false;

	// Khời tạo lại chướng ngại vật phía phải màn hình
	if (this->posX <= -100) {
		posY = random(150, 150);
		posX = 700;
	}
	top->setPosY(this->posY + 100);
	bottom->setPosY(this->posY - 100);
}

void Obstacle::render() {
	this->top->render();
	this->bottom->render();
}
﻿#pragma once

#include<iostream>
#include<string>

#include"Obstacle.h"
#include"Bird.h"

using namespace std;

namespace game {

	static bool isFirstStart = true; // Trạng thái để bắt đầu trò chơi và kết thúc trò chơi.
	static int fps = 32; // Trò chơi được dựng với 32 khung hình / s.
	static Obstacle *ob[4]; // khởi tạo vật cản
	static Bird *b; // khởi tạo đối tượng chim
	static Bird *b1;
	static int point;  // Điểm của người chơi
	static int highestPoint = 0; // Điểm cao nhất của người chơi
	static string mark; // chuỗi hiển thị điểm.
	void init();//Khởi tạo Chim, Ống nước và vật cản , điểm người chơi ......
	void display();//Hiển thị ra màn hình
	void reshape(int w, int h);//Hệ tọa độ và phép chiếu
	void inputProcess(unsigned char key, int, int);//Thao tác xử lý bàn phím
	void drawPoint();//Hiển thị điểm
	void gameOver();//kết thúc trò chơi
	void startGame();//Bắt đầu trò chơi
	void timer(int);// Vòng lặp thời gian
	void CountFrame();// Đếm Frame thực tế
	bool collisionWithTop(Pillar *p, Bird *b); // xác định trạng thái va chạm giữ chim và ống nước ở trên
	bool collisionWithBottom(Pillar *p, Bird *b);  // xác định trạng thái va chạm giữ chim và ống nước ở dưới 

}


